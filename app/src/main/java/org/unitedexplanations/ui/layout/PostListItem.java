package org.unitedexplanations.ui.layout;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apmem.tools.layouts.FlowLayout;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.unitedexplanations.R;
import org.unitedexplanations.controller.IntentController;
import org.unitedexplanations.dagger.component.StyleComponent;
import org.unitedexplanations.model.wpjson.embed.WpTerm_;
import org.unitedexplanations.model.wpjson.main.Post;
import org.unitedexplanations.system.App;
import org.unitedexplanations.ui.activity.AuthorActivity;
import org.unitedexplanations.ui.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by victor on 6/7/17.
 */

public class PostListItem extends FrameLayout implements View.OnClickListener{

    private final Picasso picasso;
    private final StyleComponent styleComponent;
    private Post post;

    @BindView(R.id.post_image)
    ImageView postImage;

    @BindView(R.id.post_title)
    CustomTextView name;

    @BindView(R.id.post_excrept)
    CustomTextView postExcrept;

    @BindView(R.id.categories_flow_layout)
    FlowLayout categoriesFlowLayout;

    @BindView(R.id.author_mini_image)
    CircleImageView authorCircleImageView;

    @BindView(R.id.author_mini_name)
    CustomTextView authorNameTextView;

    @BindView(R.id.share_main_button)
    ImageButton shareMainButton;

    @BindView(R.id.post_date)
    CustomTextView postDateTV;



    public PostListItem(Context context) {
        super(context);

        this.picasso = ((App)getContext().getApplicationContext()).getNetComponent().getPicasso();
        this.styleComponent = ((App) getContext().getApplicationContext()).getStyleComponent();

        inflate(getContext(), R.layout.list_item_post_layout, this);
        ButterKnife.bind(this);
    }

    public void setPost(final Post post) {
        this.post = post;

        //Load post image
        String url = getResources().getString(R.string.BASE_URL) + "wp-content/uploads/";
        if (post.getEmbedded().getWpFeaturedmedia() != null) url = post.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getSizes().getFull() == null ? url + post.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getFile() : post.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getSizes().getFull().getSourceUrl();
        picasso.load(url).placeholder(null).into(postImage);

        //Set the date asynchronous
        new SetDateTask().execute();

        //Add the categories to FlowLayout
        categoriesFlowLayout.removeAllViews();

        //Donwload the author image
        picasso.load(post.getEmbedded().getAuthor().get(0).getAvatarUrls().get96()).placeholder(getResources().getDrawable(R.drawable.user_default)).into(authorCircleImageView);

        //Share button laucher
        shareMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLink();
            }
        });

        //Set text categories
        setCategories(post);

        //Set text and font type asynchronous to improve the speed
        final Runnable r = new Runnable() {
            @SuppressWarnings("deprecation")
            public void run() {
                //Set texts
                authorNameTextView.setText(post.getEmbedded().getAuthor().get(0).getName());
                name.setText(Html.fromHtml(post.getTitle().getRendered()));
                postExcrept.setText(Html.fromHtml(post.getExcerpt().getRendered()));

                authorNameTextView.setSemiBold(2);
                name.setExtraBold(2);
                postExcrept.setLight(3);
            }};
        r.run();



        //Perform on click to launch activities
        this.setOnClickListener(this);
    }

    private void setCategories(final Post post) {
        final Runnable r = new Runnable() {
            public void run() {
                //get(0) are categories get(1) are tags
                List<WpTerm_> categories = post.getEmbedded().getWpTerm() != null? post.getEmbedded().getWpTerm().get(0):new ArrayList<WpTerm_>();
                for (final WpTerm_ category: categories){

                    //Asynchronously to improve the speed
                    View view = View.inflate(getContext(), R.layout.category_item, null);
                    final CustomTextView categoryNameTv = view.findViewById(R.id.category_name);
                    categoryNameTv.setText(category.getName());
                    categoryNameTv.setSemiBold(2);

                    categoriesFlowLayout.addView(view);
                }
            }};
        r.run();
    }



    @Optional
    @OnClick(R.id.author_mini_layout)
    public void onClick(View v) {

        Activity activity = (Activity) getContext();
        //In the author activity you can't click the athors link to not have an unsense bucle
        if (v.getId() == R.id.author_mini_layout && activity.getClass() != AuthorActivity.class){
            IntentController.launchAuthorActivity(activity,post,this.authorCircleImageView);
        }else {
            IntentController.launchPostActivity(activity,post,this.postImage);
        }
    }



    private void shareLink(){

       IntentController.shareString(getContext(),post.getLink());
    }

    private class SetDateTask extends AsyncTask<Void, Integer, String> {

        /*
        Do the math asynchronous
         */
        @Override
        protected String doInBackground(Void... params) {
            String date;
            DateTime postDate = new DateTime(post.getDate());
            Duration duration = new Duration(postDate, new DateTime());

            if (duration.getStandardDays() > 365){
                date = "" + duration.getStandardDays()/365 + "a";
            }else if (duration.getStandardDays() > 30){
                date = "" + duration.getStandardDays()/30 + "m";
            }else if (duration.getStandardDays() > 0){
                date = "" + duration.getStandardDays() + "d";
            }else if (duration.getStandardHours() > 0){
                date = "" + duration.getStandardHours() + "h";
            }else if (duration.getStandardMinutes() > 0){
                date = "" + duration.getStandardMinutes()+ "min";
            }else{
                date = "" +duration.getStandardSeconds()+"s";
            }

            return date;

        }


        /*
        Set the date text
         */
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            postDateTV.setText(result);
            postDateTV.setSemiBold(2);
        }

    }

}
