package org.unitedexplanations.ui.view;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.unitedexplanations.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by victor on 14/7/17.
 */

public class RefreshFooterView extends FrameLayout {

    @BindView(R.id.refresh_advice_click)
    LinearLayout refreshAdviceLayout;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    public RefreshFooterView(@NonNull Context context) {
        super(context);
        initView();
    }

    public RefreshFooterView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public RefreshFooterView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.post_footer_view, null);
        addView(view);
        ButterKnife.bind(this,view);
    }

    public void showProgress(){
        progressBar.setVisibility(VISIBLE);
        refreshAdviceLayout.setVisibility(GONE);
    }

    public void showRefresh() {
        progressBar.setVisibility(GONE);
        refreshAdviceLayout.setVisibility(VISIBLE);
    }

    public LinearLayout getRefreshAdviceLayout(){
        return this.refreshAdviceLayout;
    }
}
