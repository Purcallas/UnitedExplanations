package org.unitedexplanations.ui.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import org.unitedexplanations.dagger.libs.CustomTypeface;
import org.unitedexplanations.system.App;

/**
 * Created by victor on 22/7/17.
 */

public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    private CustomTypeface customTypeface;


    public CustomTextView(Context context) {
        super(context);
        this.customTypeface = ((App) context.getApplicationContext()).getStyleComponent().getCustomTypeface();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.customTypeface = ((App) context.getApplicationContext()).getStyleComponent().getCustomTypeface();

    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.customTypeface = ((App) context.getApplicationContext()).getStyleComponent().getCustomTypeface();
    }

    public void setBold(){
        this.setTypeface(customTypeface.getBold());
    }

    public void setBoldItalic(){
        this.setTypeface(customTypeface.getBoldItalic());
    }

    public void setExtraBold(){
        this.setTypeface(customTypeface.getExtraBold());
    }

    public void setExtraBoldItalic(){
        this.setTypeface(customTypeface.getExtraBoldItalic());

    }

    public void setItalic(){
        this.setTypeface(customTypeface.getItalic());

    }

    public void setLight(){
        this.setTypeface(customTypeface.getItalic());
    }

    public void setLightItalic(){
        this.setTypeface(customTypeface.getLightItalic());
    }

    public void setRegular(){
        this.setTypeface(customTypeface.getRegular());
    }

    public void setSemiBold(){
        this.setTypeface(customTypeface.getSemiBold());
    }

    public void setSemiBoldItalic(){
        this.setTypeface(customTypeface.getSemiBoldItalic());
    }

    public void setBold(int typeFace){
       this.setTypeface(customTypeface.getBold(typeFace));
    }

    public void setBoldItalic(int typeFace){
        this.setTypeface(customTypeface.getBoldItalic(typeFace));
    }

    public void setExtraBold(int typeFace){
        this.setTypeface(customTypeface.getExtraBold(typeFace));
    }

    public void setExtraBoldItalic(int typeFace){
        this.setTypeface(customTypeface.getExtraBoldItalic(typeFace));

    }

    public void setItalic(int typeFace){
        this.setTypeface(customTypeface.getItalic(typeFace));

    }

    public void setLight(int typeFace){
        this.setTypeface(customTypeface.getLight(typeFace));
    }

    public void setLightItalic(int typeFace){
        this.setTypeface(customTypeface.getLightItalic(typeFace));
    }

    public void setRegular(int typeFace){
        this.setTypeface(customTypeface.getRegular(typeFace));
    }

    public void setSemiBold(int typeFace){
        this.setTypeface(customTypeface.getSemiBold(typeFace));
    }

    public void setSemiBoldItalic(int typeFace){
        this.setTypeface(customTypeface.getSemiBoldItalic(typeFace));
    }

}
