package org.unitedexplanations.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.unitedexplanations.R;
import org.unitedexplanations.controller.LinkInterceptorController;
import org.unitedexplanations.dagger.component.StyleComponent;
import org.unitedexplanations.system.App;

/**
 * Created by victor on 15/7/17.
 */

@SuppressLint("SetJavaScriptEnabled")
public class LocalStyledWebView extends WebView{

    private StyleComponent styleComponent;

    public LocalStyledWebView(Context context) {
        super(context);
        styleComponent = ((App) getContext().getApplicationContext()).getStyleComponent();

    }

    public LocalStyledWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        styleComponent = ((App) getContext().getApplicationContext()).getStyleComponent();

    }

    public LocalStyledWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        styleComponent = ((App) getContext().getApplicationContext()).getStyleComponent();


    }

    //If only needs the content
    public void setStringData(String content){
        content = setContent(content);
        String html = buildTheHTML(content);
        loadHTML(html);
    }

    private String setContent(String content) {
        //Fix images
        content = content.replaceAll("width: [0-9]{3}px","");
        content = content.replaceAll("width: [0-9]{4}px","");
        return content;
    }

    private String buildTheHTML(String postContent){
        return "<!DOCTYPE html><head>"+getCSS()+"</head><html><body>"+postContent+"</body></html>";
    }

    private void loadHTML(String html){
        //Prevent text selection with long click
        this.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        this.setLongClickable(false);

        //Set web client
        this.setWebChromeClient(new WebChromeClient());
        this.getSettings().setDefaultTextEncodingName("utf-8");

        //Allow Javascript
        this.getSettings().setJavaScriptEnabled(true);

        //Load data
        this.loadDataWithBaseURL("",html,"text/html","utf-8",null);

        //Connect the link interceptor
        this.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //Open In new Window
                 if (url.startsWith(getContext().getResources().getString(R.string.BASE_URL))) {
                     if (urlIsImage(url)){
                         //Open images into gallery
                         imageGalleryLauncher(url);
                     }else{
                         //The link interceptor manage the necesary activity
                         LinkInterceptorController lic = new LinkInterceptorController(getContext());
                         lic.parseLink(url);

                     }
                    return true;
                }
                //If the link its not from united lauch in external browser
                externalBrowserLauncher(url);
                return true;
            }
        });
    }

    private boolean urlIsImage(String url){
        return (url.endsWith("png") ||url.endsWith("jpg") || url.endsWith("jpeg"));
    }

    private void imageGalleryLauncher(String url){
        if(Build.VERSION.SDK_INT>=21) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.parse(url);
            intent.setDataAndType(uri, "image/*");
            getContext().getApplicationContext().startActivity(intent);
        }
    }

    private void externalBrowserLauncher(String url){
        //Load externally other resources
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        getContext().startActivity(i);
    }

    private String getCSS(){
        String css = "<style type='text/css'>";

        //Image in full screen for mobiles
        String img = getResources().getBoolean(R.bool.isTablet)?"img{display:block;margin:0 auto; text-align: center; height: auto !important;max-width: 100% !important;}":"img{display:block;margin:0 auto; text-align: center; height: auto !important;width: 100% !important;}";
        //Add fontFaces
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getBoldString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getBoldItalicString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getExtraBoldString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getExtraBoldItalicString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getLightItalicString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getLightString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getRegularString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getSemiBoldItalicString());
        css += getfontFaceWithFontName(styleComponent.getCustomTypeface().getSemiBoldString());
        css += "iframe{display: block;max-width:100%;margin-top:10px;}" +
                "*{color: #656565; font-size:"+getResources().getString(R.string.size_webview_default)+"; line-height: 160%; font-family: "+ styleComponent.getCustomTypeface().getRegularString().split("\\.")[0]+" ;}" +
                "h1{font-size:"+getResources().getString(R.string.size_webview_h1)+";}"+
                "h2{font-size::"+getResources().getString(R.string.size_webview_h2)+"}"+
                "h3{font-size::"+getResources().getString(R.string.size_webview_h3)+"}"+
                img +
                "div{text-align: center; height: auto !important;max-width: 100% !important;}" +
                "p{font-size:"+getResources().getString(R.string.size_webview_p)+";color: #656565; font-family: "+ styleComponent.getCustomTypeface().getRegularString().split("\\.")[0]+" ;}" +
                "blockquote p em{font-style: normal; font-size:"+getResources().getString(R.string.size_webview_blockquote_p_em)+";color: #656565;font-family: "+ styleComponent.getCustomTypeface().getSemiBoldString().split("\\.")[0]+" ;}" +
                "em{font-style: normal; font-size:"+getResources().getString(R.string.size_webview_em)+";color: #656565;font-family: "+ styleComponent.getCustomTypeface().getItalicString().split("\\.")[0]+" ;}" +
                "blockquote {border-left: none ; display: block; margin-left: 0px; margin-right: 0px; margin-bottom:10px;}" +
                "ul {color: #656565; list-style: none; font-size:"+getResources().getString(R.string.size_webview_ul)+";  /* Remove list bullets */padding: 0px; margin:0;}" +
                "li{padding-top: 10px; padding-bottom:10px; font-family: "+ styleComponent.getCustomTypeface().getSemiBoldString().split("\\.")[0]+"}" +
                "span strong{color: #454545; font-family: "+ styleComponent.getCustomTypeface().getBoldString().split("\\.")[0]+"}" +
                "strong{color: #454545; font-family: "+ styleComponent.getCustomTypeface().getSemiBoldString().split("\\.")[0]+"}" +
                "a{font-size:"+getResources().getString(R.string.size_webview_a)+";color: #d92a28; text-decoration: none; font-family: "+ styleComponent.getCustomTypeface().getSemiBoldString().split("\\.")[0]+"}" +
                ".wp-caption {border-bottom: 1px solid black;border-top: 1px solid #757575; background-color: #f5f5f5;}" +
                ".wp-caption-text {font-size:"+getResources().getString(R.string.size_webview_wcaption)+";color: #757575; margin:5px; font-family: "+ styleComponent.getCustomTypeface().getItalicString().split("\\.")[0]+" ;}" +
                "</style>";
        return css;
    }

    private String getfontFaceWithFontName(String fontName){
        return "@font-face{font-family:'"+fontName.split("\\.")[0]+"'; src: url('file:///android_asset/font/"+fontName+"')}";
    }
}
