package org.unitedexplanations.ui.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.unitedexplanations.R;
import org.unitedexplanations.controller.ActivityNetworkController;
import org.unitedexplanations.model.wpjson.main.Post;
import org.unitedexplanations.ui.layout.PostListItem;
import org.unitedexplanations.ui.view.CustomTextView;
import org.unitedexplanations.ui.view.RefreshFooterView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;

/**
 * Created by victor on 6/7/17.
 */

public class PostsByAuthorAdapter extends BaseAdapter {

    private final List<Post> postList = new ArrayList<>(0);
    private final Activity context;
    private final ActivityNetworkController activityNetworkController;
    private final RefreshFooterView footer;
    private int authorId;

    @BindView(R.id.post_title)
    CustomTextView postName;




    public PostsByAuthorAdapter(Activity mainActivity, ActivityNetworkController activityNetworkController, RefreshFooterView footerView, int authorId) {
        this.context = mainActivity;
        this.activityNetworkController = activityNetworkController;
        this.footer = footerView;
        this.authorId = authorId;
        setFooterAction(this.footer,this.activityNetworkController, this.authorId, this);
    }

    private void setFooterAction(RefreshFooterView footer, final ActivityNetworkController activityNetworkController, final int authorId, final PostsByAuthorAdapter postsAdapter){
        footer.getRefreshAdviceLayout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityNetworkController.getAuthorsRefresh(postsAdapter,authorId);
            }
        });
    }

    @Override
    public int getCount() {
        return postList.size();
    }

    @Override
    public Post getItem(int position) {
        return postList.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        return postList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (postList.size()-1 == position){
            activityNetworkController.getAuthors(this,authorId);
        }

        final PostListItem postListItem;
        if(convertView == null) {
            postListItem = new PostListItem(context);
        } else {
            postListItem = PostListItem.class.cast(convertView);
        }

        final Post post = postList.get(position);
        postListItem.setPost(post);

        return postListItem;
    }

    public void addData(Collection<Post> githubRepos) {
        //Set the footer state
        setFooter(githubRepos);
        //postList.clear();
        if(githubRepos != null) {
            postList.addAll(githubRepos);
        }
        notifyDataSetChanged();
    }

    private void setFooter(Collection<Post> githubRepos) {
        if (githubRepos == null){
            this.footer.setVisibility(View.VISIBLE);
            this.footer.showRefresh();
        }else if(githubRepos.size() < 10){
            this.footer.setVisibility(View.GONE);
        }else{
            this.footer.setVisibility(View.VISIBLE);
            this.footer.showProgress();
        }
    }


}
