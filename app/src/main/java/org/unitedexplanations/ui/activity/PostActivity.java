package org.unitedexplanations.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import com.squareup.picasso.Picasso;

import org.apmem.tools.layouts.FlowLayout;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.unitedexplanations.R;
import org.unitedexplanations.controller.IntentController;
import org.unitedexplanations.dagger.component.StyleComponent;
import org.unitedexplanations.model.wpjson.embed.WpTerm_;
import org.unitedexplanations.model.wpjson.main.Post;
import org.unitedexplanations.system.App;
import org.unitedexplanations.ui.view.CustomTextView;
import org.unitedexplanations.ui.view.LocalStyledNestedWebView;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by victor on 7/7/17.
 */

public class PostActivity extends SnackBarReceiverAppCompatActivity {

    @BindView(R.id.post_image)
    ImageView imageView;
    @BindView(R.id.main_appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.main_toolbar)
    Toolbar toolbar;
    @BindView(R.id.post_excrept)
    CustomTextView postExcrept;
    @BindView(R.id.post_text)
    LocalStyledNestedWebView webView;
    @BindView(R.id.post_title)
    CustomTextView postTitle;
    @BindView(R.id.author_mini_name)
    CustomTextView authorMiniName;
    @BindView(R.id.author_mini_image)
    ImageView authorMiniImage;
    @BindView(R.id.post_date)
    CustomTextView postDateTV;
    @BindView(R.id.categories_flow_layout)
    FlowLayout categoriesView;
    @Nullable
    @BindView(R.id.toolbar_title)
    ImageView logoToolbar;


    @BindString(R.string.BASE_URL)
    String BASE_URL;

    @Inject
    Picasso picasso;

    private Post post;
    private Menu menu;
    private boolean isMaxDepth;     //To prevent infinite activity calling


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_activity);

        //Receive info from intent parent
        Intent intent = this.getIntent();
        String jsonPost = intent.getStringExtra("post");
        this.isMaxDepth = intent.getBooleanExtra("max_depth",false);
        this.post = new Gson().fromJson(jsonPost,Post.class);
        //String imageUrl = post.getEmbedded().get

        //Bind ButterKnife
        ButterKnife.bind(this);

        //Inject Dagger2
        ((App) getApplicationContext()).getNetComponent().inject(this);

        //Set proper actionbar with home button
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Add appBar action about share button
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (menu != null){
                    //Action Bar collapsed
                    if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                        MenuItem share = menu.findItem(R.id.action_share);
                        share.setVisible(true);
                        logoToolbar.setVisibility(View.VISIBLE);
                        //Action Bar not collapsed
                    } else {
                        MenuItem share = menu.findItem(R.id.action_share);
                        share.setVisible(false);
                        logoToolbar.setVisibility(View.GONE);


                    }
                }
            }
        });

        String imageUrl = this.getResources().getString(R.string.BASE_URL) + "wp-content/uploads/";
        if (post.getEmbedded().getWpFeaturedmedia() != null) imageUrl = post.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getSizes().getFull() == null ? imageUrl + post.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getFile() : post.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getSizes().getFull().getSourceUrl();

        //Download the titleTV image
        picasso.load(imageUrl).placeholder(R.drawable.united_gray).into(imageView);

        //Donwload the author image
        picasso.load(post.getEmbedded().getAuthor().get(0).getAvatarUrls().get96()).into(authorMiniImage);

        //Set the propper texts
        setTexts();

        //Set and load the webview
        this.webView.setStringData(post.getContent().getRendered());

    }

    @SuppressWarnings("deprecation")
    private void setTexts() {

        //Fix excrept
        String parsedExcrept = post.getExcerpt().getRendered().replace("LEER MÁS","");

        //Date format to show
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");
        DateTime date = new DateTime(post.getDate());

        //Set texts
        postDateTV.setText(fmt.print(date));
        postExcrept.setText(Html.fromHtml(parsedExcrept));
        postTitle.setText((Html.fromHtml(post.getTitle().getRendered())));
        authorMiniName.setText(post.getEmbedded().getAuthor().get(0).getName());

        //Set typefaces
        postDateTV.setRegular(2);
        postExcrept.setSemiBoldItalic(3);
        postTitle.setSemiBold(2);
        authorMiniName.setSemiBold(2);


        //Add categories text by adding text views to the FlowLayout
        //get(0) are categories get(1) are tags
        List<WpTerm_> categories = post.getEmbedded().getWpTerm() != null? post.getEmbedded().getWpTerm().get(0):new ArrayList<WpTerm_>();
        for (final WpTerm_ category: categories){
            View view = View.inflate(this, R.layout.category_item,null);
            CustomTextView categoryNameTv = view.findViewById(R.id.category_name);
            categoryNameTv.setText(category.getName());
            categoryNameTv.setSemiBold(2);
            /* Not neccesary behaviour
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Category", category.getName());
                }
            });
            */
            categoriesView.addView(view);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean  onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                //onBackPressed();
                finish();
                return true;
            case R.id.action_share:
                shareLink();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    @Optional
    @OnClick({R.id.action_share,R.id.share_fab, R.id.author_mini_layout})
    public void onClick(View v) {
        if (v.getId() == R.id.author_mini_layout){
            //Prevent infinite bucle open activities
            if (!isMaxDepth){
                authorActivityIntent();
            }
        }else{
            shareLink();
        }
    }

    private void shareLink(){
        IntentController.shareString(this,post.getLink());
    }

    private void authorActivityIntent(){
        IntentController.launchAuthorActivity(this,post,authorMiniImage);
    }

}
