package org.unitedexplanations.ui.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.unitedexplanations.R;
import org.unitedexplanations.controller.ActivityNetworkController;
import org.unitedexplanations.system.App;
import org.unitedexplanations.ui.view.CustomTextView;
import org.unitedexplanations.ui.view.LocalStyledWebView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by victor on 15/7/17.
 */

public class SimpleWebViewActivity extends SnackBarReceiverAppCompatActivity {

    @BindView(R.id.web_view)
    LocalStyledWebView webView;

    @BindView(R.id.simple_web_view_image)
    ImageView imageView;

    @BindView(R.id.simple_web_view_image_title)
    CustomTextView imageTitle;

    @BindView(R.id.simple_web_view_title)
    CustomTextView titleTV;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Picasso picasso;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_simple_web);

        //Bind ButterKnife
        ButterKnife.bind(this);

        //noinspection ConstantConditions
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        picasso = ((App) getApplicationContext()).getNetComponent().getPicasso();

        Intent intent = this.getIntent();

        int id = intent.getIntExtra("id",-1);
        String slug = intent.getStringExtra("slug");
        String imageUrl = intent.getStringExtra("imageUrl");
        String content = intent.getStringExtra("content");
        String title = intent.getStringExtra("title");

        if (imageUrl != null || content != null || title != null) updateContent(imageUrl,title,content);

        //Launch the accurate method
        ActivityNetworkController activityNetworkController = new ActivityNetworkController(this);

        if (content !=null){
            updateContent(imageUrl,title,content);
        }else if (id != -1 || slug != null){
            if (id != -1) activityNetworkController.updatePageById(this,id);
            else activityNetworkController.updatePageBySlugAndWebView(this,slug);
        }
    }


    public void updateContent(String imageUrl, String title, String content){

        // Add image or title if they have it
        if (imageUrl != null){
            if(Build.VERSION.SDK_INT<21){
                picasso.load(imageUrl).placeholder(getResources().getDrawable(R.drawable.united_gray)).into(imageView);
            }else{
                picasso.load(imageUrl).placeholder(getDrawable(R.drawable.united_gray)).into(imageView);
            }
            if (title != null) this.imageTitle.setText(title);
        }else if (title!=null){
            this.titleTV.setText(title);
        }

        if (content != null){
            webView.setStringData(content);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                //finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
