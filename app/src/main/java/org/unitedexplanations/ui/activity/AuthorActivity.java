package org.unitedexplanations.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;


import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.unitedexplanations.R;
import org.unitedexplanations.controller.ActivityNetworkController;
import org.unitedexplanations.dagger.component.StyleComponent;
import org.unitedexplanations.model.wpjson.embed.Author_;
import org.unitedexplanations.model.wpjson.main.Post;
import org.unitedexplanations.system.App;
import org.unitedexplanations.ui.adapter.PostsByAuthorAdapter;
import org.unitedexplanations.ui.view.CustomTextView;
import org.unitedexplanations.ui.view.RefreshFooterView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by victor on 12/7/17.
 */

public class AuthorActivity extends SnackBarReceiverAppCompatActivity {

    @BindView(R.id.main_toolbar)
    Toolbar toolbar;

    @BindView(R.id.author_mini_image)
    CircleImageView authorImageView;

    @BindView(R.id.author_description)
    CustomTextView authorDescription;

    @BindView(R.id.author_name)
    CustomTextView authorName;

    @BindView(R.id.repo_home_list)
    ListView listView;


    @Inject
    Picasso picasso;

    private StyleComponent styleComponent;
    private Post post;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.author_layout);

        //Init ButterKnife
        ButterKnife.bind(this);

        ((App) getApplicationContext()).getNetComponent().inject(this);



        Intent intent = this.getIntent();
        String jsonPost = intent.getStringExtra("post");
        this.post = new Gson().fromJson(jsonPost,Post.class);

        //Build the footer for the listView
        RefreshFooterView footerView = new RefreshFooterView(this);
        listView.addFooterView(footerView);

        //Inject Dagger2
        ((App) getApplicationContext()).getNetComponent().inject(this);
        styleComponent = ((App) getApplicationContext()).getStyleComponent();


        setSupportActionBar(toolbar);

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Build controller
        ActivityNetworkController activityNetworkController = new ActivityNetworkController(this);

        //Build the adapter
        PostsByAuthorAdapter postsAdapter = new PostsByAuthorAdapter(this, activityNetworkController, footerView, post.getAuthor());

        //Set the posts adapter
        listView.setAdapter(postsAdapter);

        activityNetworkController.getAuthors(postsAdapter,post.getAuthor());

        picasso.load(post.getEmbedded().getAuthor().get(0).getAvatarUrls().get96()).into(authorImageView);

        //Set the texts
        setTexts();

        //Set the fonts
        setFonts();

    }

    @SuppressWarnings("deprecation")
    private void setTexts(){
        Author_ author = post.getEmbedded().getAuthor().get(0);

        /*
        if(Build.VERSION.SDK_INT<21){
            authorDescription.setText(Html.fromHtml(author.getDescription()));
        }else{
            //authorDescription.setText(Html.fromHtml("<b>"+author.getName()+"</b><br /><br />"+author.getDescription()));
        }
        */
        authorDescription.setText(Html.fromHtml(author.getDescription()));
        authorName.setText(author.getName());
    }

    private void setFonts(){
        authorDescription.setLight(3);
        authorName.setSemiBold(2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return true;
    }

    @Override
    public boolean  onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

}
