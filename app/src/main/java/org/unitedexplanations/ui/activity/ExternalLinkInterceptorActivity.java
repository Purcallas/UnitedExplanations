package org.unitedexplanations.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import org.unitedexplanations.R;

/**
 * Created by victor on 17/7/17.
 */

public class ExternalLinkInterceptorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.external_link_interceptor_activity);

        Intent intent = getIntent();
        Uri data = intent.getData();

        intent = new Intent(this, MainActivity.class);
        if (data != null){
            String url = data.toString();
            intent.putExtra("link",url);
        }
        startActivity(intent);
        finish();

    }
}
