package org.unitedexplanations.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by victor on 16/7/17.
 */

public class SnackBarReceiverAppCompatActivity extends AppCompatActivity {

    private BroadcastReceiver mMessageReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Other stuff.

        final SnackBarReceiverAppCompatActivity snackBarReceiverAppCompatActivity = this;
        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra("snackbar_message");
                Snackbar mySnackbar = Snackbar.make((snackBarReceiverAppCompatActivity).getWindow().getDecorView().getRootView(),
                        message, Snackbar.LENGTH_SHORT);
                mySnackbar.show();
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("EVENT_SNACKBAR"));
    }


}
