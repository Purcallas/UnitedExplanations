package org.unitedexplanations.ui.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import org.unitedexplanations.R;
import org.unitedexplanations.controller.ActivityNetworkController;
import org.unitedexplanations.controller.LinkInterceptorController;
import org.unitedexplanations.ui.adapter.PostsAdapter;
import org.unitedexplanations.ui.view.RefreshFooterView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends SnackBarReceiverAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    PostsAdapter postsAdapter;
    ActivityNetworkController activityNetworkController;

    @BindView(R.id.toolbar) public Toolbar toolbar;
    @BindView(R.id.drawer_layout) public DrawerLayout drawer;
    @BindView(R.id.nav_view) public NavigationView navigationView;
    @BindView(R.id.repo_home_list) public ListView listView;
    @BindView(R.id.repo_home_list_swipe_refresh) public SwipeRefreshLayout swipeRefreshLayout;

    ActionBarDrawerToggle toggle;


    //Drawables
    @BindDrawable(R.drawable.logo_letras)
    Drawable appIcon;

    //Strings
    @BindString(R.string.app_name)
    String appName;
    @BindString(R.string.menu_item_facebook_link)
    String facebookLink;
    @BindString(R.string.menu_item_twitter_link)
    String twitterLink;
    @BindString(R.string.menu_item_google_link)
    String googlePlusLink;
    @BindString(R.string.menu_item_youtube_link)
    String youtubeLink;
    @BindString(R.string.menu_item_linkedin_link)
    String linkedinLink;
    @BindString(R.string.mail_info_united_explanations)
    String infoMail;
    @BindString(R.string.mail_collborate_subject)
    String collaborateSubject;
    @BindString(R.string.mail_collaborate_content)
    String collaborateContent;


    //Color
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindColor(R.color.colorAccent)
    int colorAccent;


    //Menu
    Menu menu;
    MenuItem menuHome;
    Map<MenuItem,Integer> mapTaggedMenuItems;
    RefreshFooterView footerView;

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init ButterKnife
        ButterKnife.bind(this);

        //Set toolbar
        setSupportActionBar(toolbar);
        //Remove titleTV
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        //Build controller
        activityNetworkController = new ActivityNetworkController(this);

        //Build the footer for the listView
        footerView = new RefreshFooterView(this);
        listView.addFooterView(footerView);

        //Build the adapter
        postsAdapter = new PostsAdapter(this, activityNetworkController,footerView);

        //Set the posts adapter
        listView.setAdapter(postsAdapter);


        //Set united icon
        //toolbar.setOverflowIcon(appIcon);
        //toolbar.setNavigationIcon(appIcon);
        //toolbar.setLogo(appIcon);
        //toolbar.setTitle("UnitedExplanations");


        //Build the navigation bar
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //getSupportActionBar().setHomeAsUpIndicator(appIcon);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(this);

        //Style
        swipeRefreshLayout.setColorSchemeColors(colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                activityNetworkController.getPostsRefreshWithSwipeRefresh(postsAdapter,swipeRefreshLayout);

            }
        });

        //Init menu tag map
        mapTaggedMenuItems = new HashMap<>();

        //Check if external link intent arrived
        checkExternalLink(getIntent());

        activityNetworkController.getPosts(postsAdapter);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkExternalLink(intent);
    }


    private void checkExternalLink(Intent intent){
        String externalLink = intent.getStringExtra("link");
        if (externalLink != null){
            LinkInterceptorController linkInterceptorController = new LinkInterceptorController(this);
            linkInterceptorController.parseLink(externalLink);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        this.menu = menu;
        this.menuHome = menu.getItem(0);

        //To have the united explanations tag with name associated
        buildMenuItemTagMap();

        return true;
    }



    private void buildMenuItemTagMap() {
        //Build map with items
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_home),null);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_afrique),17);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_north_america),13);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_latin_America),14);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_asia),16);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_culture),1517);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_human_rights),264);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_global_development),7);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_docs),4312);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_economy),6);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_europe),12);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_gender), 1694);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_human_rights),264);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_infography),5949);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_maps),6830);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_enviroment),9);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_oceanie),5411);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_midle_orient),15);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_peace),8);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_politics),10);
        mapTaggedMenuItems.put(this.menu.findItem(R.id.nav_society),1516);
    }

    @Override
    public boolean  onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.nav_home:
                setSelectorTitle(R.id.nav_home);
                return true;
            case R.id.nav_afrique:
                setSelectorTitle(R.id.nav_afrique);
                return true;
            case R.id.nav_asia:
                setSelectorTitle(R.id.nav_asia);
                return true;
            case R.id.nav_culture:
                setSelectorTitle(R.id.nav_culture);
                return true;
            case R.id.nav_docs:
                setSelectorTitle(R.id.nav_docs);
                return true;
            case R.id.nav_economy:
                setSelectorTitle(R.id.nav_economy);
                return true;
            case R.id.nav_enviroment:
                setSelectorTitle(R.id.nav_enviroment);
                return true;
            case R.id.nav_europe:
                setSelectorTitle(R.id.nav_europe);
                return true;
            case R.id.nav_gender:
                setSelectorTitle(R.id.nav_gender);
                return true;
            case R.id.nav_global_development:
                setSelectorTitle(R.id.nav_global_development);
                return true;
            case R.id.nav_human_rights:
                setSelectorTitle(R.id.nav_human_rights);
                return true;
            case R.id.nav_infography:
                setSelectorTitle(R.id.nav_infography);
                return true;
            case R.id.nav_latin_America:
                setSelectorTitle(R.id.nav_latin_America);
                return true;
            case R.id.nav_maps:
                setSelectorTitle(R.id.nav_maps);
                return true;
            case R.id.nav_midle_orient:
                setSelectorTitle(R.id.nav_midle_orient);
                return true;
            case R.id.nav_north_america:
                setSelectorTitle(R.id.nav_north_america);
                return true;
            case R.id.nav_oceanie:
                setSelectorTitle(R.id.nav_oceanie);
                return true;
            case R.id.nav_peace:
                setSelectorTitle(R.id.nav_peace);
                return true;
            case R.id.nav_politics:
                setSelectorTitle(R.id.nav_politics);
                return true;
            case R.id.nav_society:
                setSelectorTitle(R.id.nav_society);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setSelectorTitle(int idItem){
        MenuItem menuItem = this.menu.findItem(idItem);
        //Set item title
        this.menuHome.setTitle(menuItem.getTitle());
        //Set tag in the adapter
        postsAdapter.setTag(mapTaggedMenuItems.get(menuItem));
        activityNetworkController.getPostsRefresWithChangeMenuItemTag(postsAdapter);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_what_united) {
            Intent intent = new Intent(this, SimpleWebViewActivity.class);
            intent.putExtra("slug","about");
            startActivity(intent);
        } else if (id == R.id.nav_team) {
            Intent intent = new Intent(this, SimpleWebViewActivity.class);
            intent.putExtra("slug","donors");
            startActivity(intent);
        } else if (id == R.id.nav_support) {
            Intent intent = new Intent(this, SimpleWebViewActivity.class);
            intent.putExtra("slug","mecenas");
            startActivity(intent);
        }else if (id == R.id.nav_collaborate) {
             /* Create the Intent */
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            /* Fill it with Data */
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{infoMail});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, collaborateSubject);
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, collaborateContent);
            /* Send it off to the Activity-Chooser */
            startActivity(Intent.createChooser(emailIntent, "Envía mensaje"));
        } else if (id == R.id.nav_youtube) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeLink));
            startActivity(browserIntent);
        } else if (id == R.id.nav_linkedin) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkedinLink));
            startActivity(browserIntent);
        } else if (id == R.id.nav_twitter) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitterLink));
            startActivity(browserIntent);
        } else if (id == R.id.nav_google) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(googlePlusLink));
            startActivity(browserIntent);
        }else if (id == R.id.nav_facebook) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookLink));
            startActivity(browserIntent);
        }else if (id == R.id.nav_contact) {
            /* Create the Intent */
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            /* Fill it with Data */
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{infoMail});
            /* Send it off to the Activity-Chooser */
            startActivity(Intent.createChooser(emailIntent, "Envía mensaje"));
        }

        //Close drawer
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

}
