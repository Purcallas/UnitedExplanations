package org.unitedexplanations.model.wpjson.common;

/**
 * Created by victor on 6/7/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sizes {

    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;
    @SerializedName("medium")
    @Expose
    private Medium medium;
    @SerializedName("medium_large")
    @Expose
    private MediumLarge mediumLarge;
    @SerializedName("large")
    @Expose
    private Large large;
    @SerializedName("single-large")
    @Expose
    private SingleLarge singleLarge;
    @SerializedName("small")
    @Expose
    private Small small;
    @SerializedName("thumbnail-large")
    @Expose
    private ThumbnailLarge thumbnailLarge;
    @SerializedName("full-width-image")
    @Expose
    private FullWidthImage fullWidthImage;
    @SerializedName("full")
    @Expose
    private Full full;

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Medium getMedium() {
        return medium;
    }

    public void setMedium(Medium medium) {
        this.medium = medium;
    }

    public MediumLarge getMediumLarge() {
        return mediumLarge;
    }

    public void setMediumLarge(MediumLarge mediumLarge) {
        this.mediumLarge = mediumLarge;
    }

    public Large getLarge() {
        return large;
    }

    public void setLarge(Large large) {
        this.large = large;
    }

    public SingleLarge getSingleLarge() {
        return singleLarge;
    }

    public void setSingleLarge(SingleLarge singleLarge) {
        this.singleLarge = singleLarge;
    }

    public Small getSmall() {
        return small;
    }

    public void setSmall(Small small) {
        this.small = small;
    }

    public ThumbnailLarge getThumbnailLarge() {
        return thumbnailLarge;
    }

    public void setThumbnailLarge(ThumbnailLarge thumbnailLarge) {
        this.thumbnailLarge = thumbnailLarge;
    }

    public FullWidthImage getFullWidthImage() {
        return fullWidthImage;
    }

    public void setFullWidthImage(FullWidthImage fullWidthImage) {
        this.fullWidthImage = fullWidthImage;
    }

    public Full getFull() {
        return full;
    }

    public void setFull(Full full) {
        this.full = full;
    }

}