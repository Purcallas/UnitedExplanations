package org.unitedexplanations.system;

import android.app.Application;

import org.unitedexplanations.R;
import org.unitedexplanations.dagger.component.DaggerStyleComponent;
import org.unitedexplanations.dagger.component.StyleComponent;
import org.unitedexplanations.dagger.component.DaggerMemoryComponent;
import org.unitedexplanations.dagger.component.DaggerNetComponent;
import org.unitedexplanations.dagger.component.MemoryComponent;
import org.unitedexplanations.dagger.component.NetComponent;
import org.unitedexplanations.dagger.module.AppModule;
import org.unitedexplanations.dagger.module.ContextModule;
import org.unitedexplanations.dagger.module.InternalMemoryModule;
import org.unitedexplanations.dagger.module.NetModule;

import timber.log.Timber;

/**
 * Created by victor on 6/7/17.
 */

public class App extends Application {

    private String BASE_URL;

    private NetComponent netComponent;
    private StyleComponent styleComponent;
    private MemoryComponent memoryComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        //Set base url connection from strings
        BASE_URL = getResources().getString(R.string.BASE_URL);

        Timber.plant(new Timber.DebugTree());

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BASE_URL))
                .contextModule(new ContextModule(this))
                .build();
        styleComponent = DaggerStyleComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
        memoryComponent = DaggerMemoryComponent.builder()
                .contextModule(new ContextModule(this))
                .internalMemoryModule(new InternalMemoryModule()).build();

    }

    public NetComponent getNetComponent() {
        return netComponent;
    }
    public StyleComponent getStyleComponent() {
        return styleComponent;
    }
    public MemoryComponent getMemoryComponent(){ return memoryComponent; };
}