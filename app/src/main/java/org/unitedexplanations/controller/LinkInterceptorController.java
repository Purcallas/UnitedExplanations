package org.unitedexplanations.controller;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import org.unitedexplanations.R;
import org.unitedexplanations.ui.view.LocalStyledWebView;

/**
 * Created by victor on 16/7/17.
 */

public final class LinkInterceptorController {

    private Context context;

    //Wp-json tags
    private String authorTag;
    private String categoryTag;
    private String tagTag;
    private String postTag;
    private String pageTag;

    //United Explanations Page
    private String englishPage;
    private String collaboratorsPage;
    private String donorsPage;
    private String mecenasPage;
    private String aboutPage;
    private String contactPage;
    private String imagePage;

    // Mail info
    private String infoMail;
    private String collaborateSubject;
    private String collaborateContent;

    // App name
    private String appName;


    ActivityNetworkController activityNetworkController;




    public LinkInterceptorController(Context context) {
        this.context = context;

        authorTag = context.getResources().getString(R.string.wp_json_author);
        categoryTag = context.getResources().getString(R.string.wp_json_category);
        tagTag = context.getResources().getString(R.string.wp_json_tag);
        postTag = context.getResources().getString(R.string.wp_json_post);
        pageTag = context.getResources().getString(R.string.wp_json_page);
        englishPage = context.getResources().getString(R.string.page_english);
        aboutPage = context.getResources().getString(R.string.page_about);
        collaboratorsPage = context.getResources().getString(R.string.page_collaborate);
        donorsPage = context.getResources().getString(R.string.page_donors);
        mecenasPage = context.getResources().getString(R.string.page_mecenas);
        contactPage = context.getResources().getString(R.string.page_contact);
        imagePage = context.getResources().getString(R.string.page_image);

        infoMail = context.getResources().getString(R.string.mail_info_united_explanations);
        collaborateContent = context.getResources().getString(R.string.mail_collaborate_content);
        collaborateSubject = context.getResources().getString(R.string.mail_collborate_subject);

        appName = context.getResources().getString(R.string.app_name);

        activityNetworkController = new ActivityNetworkController(context);

    }

    public boolean parseLink(String stringUrl){

        boolean launchExternal = false;
        Uri uri = Uri.parse(stringUrl);

        if (uri.getPathSegments().size() == 0){
            launchExternal = true;
        }else if (uri.getPathSegments().contains(categoryTag)){
            launchExternal = true;
        }else if (uri.getPathSegments().contains(tagTag)){
            launchExternal = true;
        }else if (uri.getPathSegments().contains(englishPage)){
            launchExternal = true;
        }else if (uri.getPathSegments().contains(imagePage)){
            launchExternal = true;
        }else if (uri.getPathSegments().contains(pageTag) || uri.getPathSegments().contains(aboutPage) || uri.getPathSegments().contains(donorsPage) ||  uri.getPathSegments().contains(mecenasPage)){
            String slug = uri.getLastPathSegment();
            activityNetworkController.startPageBySlug(slug);
            Log.i("Page",slug);
        }else if (uri.getPathSegments().contains(collaboratorsPage)){
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{infoMail});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, collaborateSubject);
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, collaborateContent);
            context.startActivity(Intent.createChooser(emailIntent, appName));
        }else if (uri.getPathSegments().contains(contactPage)){
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{infoMail});
            context.startActivity(Intent.createChooser(emailIntent, appName));
        }else if (uri.getPathSegments().contains(authorTag)){
            //Author

            //String slug = uri.getLastPathSegment();
            //activityNetworkController.getAuthorBySlug(slug);
            //Not possible without OAuth
            launchExternal = true;
            //The post structure is /YYYY/MM/DD/slug 4 path segments
        }else if (uri.getPathSegments().size() > 3){
            //Post (usually)
            String slug = uri.getLastPathSegment();
            activityNetworkController.startPostBySlug(slug);
            launchExternal = false;
        }else{
            launchExternal = true;
        }

        return launchExternal;
    }

    public String getUrlSlug(String stringUrl){
        Uri uri = Uri.parse(stringUrl);
        return uri.getLastPathSegment();
    }
}
