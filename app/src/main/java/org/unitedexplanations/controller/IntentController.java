package org.unitedexplanations.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.widget.ImageView;

import com.google.gson.Gson;

import org.unitedexplanations.R;
import org.unitedexplanations.model.wpjson.main.Page;
import org.unitedexplanations.model.wpjson.main.Post;
import org.unitedexplanations.system.App;
import org.unitedexplanations.ui.activity.AuthorActivity;
import org.unitedexplanations.ui.activity.PostActivity;
import org.unitedexplanations.ui.activity.SimpleWebViewActivity;

/**
 * Created by victor on 18/7/17.
 */

public class IntentController {

    public static void launchAuthorActivity(Context activityContext, Post post, ImageView imageShared){
        //Build intent between activities with extra
        Intent intent = new Intent(activityContext, AuthorActivity.class) ;
        //Convert the postToJSON to send as extra
        Gson gson = new Gson();
        String jsonInString = gson.toJson(post);
        intent.putExtra("post", jsonInString);

        //Send the author image
        String transitionName = activityContext.getResources().getString(R.string.author_image_transition);

        if (imageShared != null){
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation((Activity) activityContext, imageShared, transitionName);
            activityContext.startActivity(intent, options.toBundle());
        }else{
            activityContext.startActivity(intent);
        }
    }

    public static void launchPostActivity(Context activityContext, Post post, ImageView imageShared){
        //Build intent between activities with extra
        Intent intent = new Intent(activityContext, PostActivity.class);
        //Convert the postToJSON to send as extra
        Gson gson = new Gson();
        String jsonInString = gson.toJson(post);
        intent.putExtra("post", jsonInString);
        //Prevent infinite calling author, article, author, article....
        if(activityContext.getClass() == AuthorActivity.class) intent.putExtra("max_depth", true);

        //Send the menu_post post image
        String transitionName = activityContext.getResources().getString(R.string.post_image_transition);

        if (imageShared != null){
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation((Activity) activityContext, imageShared, transitionName);
            activityContext.startActivity(intent, options.toBundle());
        }else{
            activityContext.startActivity(intent);
        }

    }

    public static void shareString(Context context, String shareString){
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, shareString);
        context.startActivity(Intent.createChooser(share, "United Explanations Android App"));
    }

    public static void startPageWithIntent(Context context, Page page){
        //Update content
        Intent intent = new Intent(context, SimpleWebViewActivity.class) ;
        //Convert the postToJSON to send as extra
        if (page != null){
            String content = page.getContent().getRendered();
            String title = page.getTitle().getRendered();
            //Load post image

            String imageUrl = getPageImageUrl(context,page);

            //Fill the intent with data
            intent.putExtra("imageUrl",imageUrl);
            intent.putExtra("content",content);
            intent.putExtra("title",title);
        }
        context.startActivity(intent);

    }

    public static void updatePageIntent(Context context, Page page){
        //Update content
        SimpleWebViewActivity simpleWebViewActivity = (SimpleWebViewActivity) context;
        if (page != null){
            String content = page.getContent().getRendered();
            String title = page.getTitle().getRendered();
            //Load post image
            String imageUrl = getPageImageUrl(context,page);
            simpleWebViewActivity.updateContent(imageUrl,title,content);
        }
    }

    private static String getPageImageUrl(Context context, Page page){
        String imageUrl = null;
        String baseUrl = context.getResources().getString(R.string.BASE_URL) + "wp-content/uploads/";
        if (page.getEmbedded().getWpFeaturedmedia() != null) imageUrl = page.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getSizes().getFull() == null ? baseUrl + page.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getFile() : page.getEmbedded().getWpFeaturedmedia().get(0).getMediaDetails().getSizes().getFull().getSourceUrl();
        return imageUrl;
    }
}
