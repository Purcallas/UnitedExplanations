package org.unitedexplanations.controller;

import android.content.Context;

import org.joda.time.DateTime;
import org.unitedexplanations.dagger.libs.InternalMemory;
import org.unitedexplanations.system.App;

/**
 * Created by victor on 21/7/17.
 */

public class MemoryController {

    private static final String MEMORY_ATTR = "page";

    private Context context;
    InternalMemory internalMemory;

    public MemoryController(Context context){
        this.context = context;
        internalMemory = ((App) context.getApplicationContext()).getMemoryComponent().getInternalMemory();
    }

    public void storeDateByTag(int tag){
        internalMemory.putString(MEMORY_ATTR+tag, new DateTime().toString());
    }

    public void storeDate(){
        internalMemory.putString(MEMORY_ATTR, new DateTime().toString());
    }

    public DateTime getLastStoreDateByTag(int tag){
        String dateTimeString = internalMemory.getString(MEMORY_ATTR+tag);
        if (dateTimeString != null){
            return DateTime.parse(dateTimeString);
        }
        return null;
    }

    public DateTime getLastStoreDate(){
        String dateTimeString = internalMemory.getString(MEMORY_ATTR);
        if (dateTimeString != null){
            return DateTime.parse(dateTimeString);
        }
        return null;
    }
}
