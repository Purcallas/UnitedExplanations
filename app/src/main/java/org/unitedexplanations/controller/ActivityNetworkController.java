package org.unitedexplanations.controller;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.unitedexplanations.R;
import org.unitedexplanations.model.wpjson.main.Page;
import org.unitedexplanations.model.wpjson.main.Post;
import org.unitedexplanations.network.UnitedExplanationsWPService;
import org.unitedexplanations.system.App;
import org.unitedexplanations.ui.activity.PostActivity;
import org.unitedexplanations.ui.activity.SimpleWebViewActivity;
import org.unitedexplanations.ui.adapter.PostsAdapter;
import org.unitedexplanations.ui.adapter.PostsByAuthorAdapter;
import org.unitedexplanations.ui.layout.PostListItem;
import org.unitedexplanations.ui.view.LocalStyledWebView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Created by victor on 7/7/17.
 */

public class ActivityNetworkController {


    Context context;

    @Inject Retrofit retrofit;
    @Inject Picasso picasso;


    int pageCounter = 1;
    boolean postGetNetworkIsAlive = true;


    public ActivityNetworkController(Context context) {
        this.context = context;

        //Inject Dependencies
        ((App) context.getApplicationContext()).getNetComponent().inject(this);

    }

    public void getPosts(final PostsAdapter postsAdapter){
        //Posts call
        Call<List<Post>> postsCall;
        Integer tag = postsAdapter.getTag();

        if (tag == null)  postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPage(pageCounter);
        else  postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPageAndTag(pageCounter,tag);

        if (postGetNetworkIsAlive && postsCall != null) {
            //Swipe to refresh is null
            postCall(postsAdapter,postsCall, null);
        }
    }


    public void getPostsRefresh(final PostsAdapter postsAdapter){

        //Posts call
        Call<List<Post>> postsCall;
        Integer tag = postsAdapter.getTag();

        //When refresh the Post network call set alive the network communication
        postGetNetworkIsAlive = true;



        //Posts call
        if (tag == null)  postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPage(pageCounter);
        else  postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPageAndTag(pageCounter,tag);
        postCall(postsAdapter,postsCall, null);

    }

    public void getPostsRefresWithChangeMenuItemTag(final PostsAdapter postsAdapter){
        this.pageCounter = 1;
        postsAdapter.clearData();
        getPostsRefresh(postsAdapter);
    }


    public void getPostsRefreshWithSwipeRefresh(final PostsAdapter postsAdapter, final SwipeRefreshLayout swipeRefreshLayout){
        this.pageCounter = 1;
        postsAdapter.clearDataBySwipeRefresh();

        //When refresh the Post network call set alive the network communication
        postGetNetworkIsAlive = true;


        //Posts call
        Call<List<Post>> postsCall;
        Integer tag = postsAdapter.getTag();

        //Posts call
        if (tag == null)  postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPage(pageCounter);
        else  postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPageAndTag(pageCounter,tag);
        postCall(postsAdapter,postsCall, swipeRefreshLayout);
    }

    public void getAuthors(final PostsByAuthorAdapter postsByAuthorAdapter, int authorId){
        //Posts call
        Call<List<Post>> postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPage(pageCounter);
        if (postGetNetworkIsAlive) {
            //Swipe to refresh is null
            authorPostsCall(postsByAuthorAdapter, authorId ,postsCall, null);
        }
    }

    public void getAuthorsRefresh(final PostsByAuthorAdapter postsByAuthorAdapter, int authorId){
        //When refresh the Post network call set alive the network communication
        postGetNetworkIsAlive = true;
        //Posts call
        Call<List<Post>> postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPage(pageCounter);
        //Swipe to refresh is null
        authorPostsCall(postsByAuthorAdapter, authorId ,postsCall, null);
    }

    public void getAuthorsRefreshWithSwipeRefresh(final PostsByAuthorAdapter postsAuthorAdapter, int authorId, final SwipeRefreshLayout swipeRefreshLayout){
        //When refresh the Post network call set alive the network communication
        postGetNetworkIsAlive = true;
        //Posts call
        Call<List<Post>> postsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPage(pageCounter);
        authorPostsCall(postsAuthorAdapter, authorId, postsCall, swipeRefreshLayout);

    }

    private void postCall(final PostsAdapter postsAdapter, Call<List<Post>> postsCall, final SwipeRefreshLayout swipeRefreshLayout) {
        //Enque the call
        postsCall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                //Set the response to the textview
                if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);

                //The maximum result of items are 10
                //If the result has less close the network
                //if (response.body() != null && response.body().size() < 10) postGetNetworkIsAlive = false;
                if (response.body() == null) {
                    postGetNetworkIsAlive = false;
                    postsAdapter.addData(null);

                    //Show message if not internet
                    if (!isNetworkAvailable(context)) {
                        //Show internet connection faliure
                        String message = context.getResources().getString(R.string.connection_issue);
                        showUIMessage(message);
                    }

                }else{
                    //Set the response to the textview
                    postsAdapter.addData(response.body());
                    pageCounter++;
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
                postGetNetworkIsAlive = false;

                //Show internet connection faliure
                String message = context.getResources().getString(R.string.connection_issue);
                showUIMessage(message);

                //Add data decide if show the loading footer
                postsAdapter.addData(null);

                //Set the error to the textviev
                Timber.i(t.toString());
            }
        });
    }




    private void authorPostsCall(final PostsByAuthorAdapter postsByAuthorAdapter, int authorId, Call<List<Post>> postsCall, final SwipeRefreshLayout swipeRefreshLayout) {
        //Posts call
        Call<List<Post>> authorsCall = retrofit.create(UnitedExplanationsWPService.class).getPostsWithPageAndAuthor(pageCounter, authorId);
        //Enque the call
        authorsCall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                //Set the response to the textview
                if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);

                if (response.body() != null && response.body().size() < 10) postGetNetworkIsAlive = false;
                if (response.body() == null) {
                    postGetNetworkIsAlive = false;
                    postsByAuthorAdapter.addData( new ArrayList<Post>());
                }else{
                    //Set the response to the textview
                    postsByAuthorAdapter.addData(response.body());
                    pageCounter++;
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                //Set the response to the textview
                if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
                postGetNetworkIsAlive = false;

                //Show internet connection faliure
                String message = context.getResources().getString(R.string.connection_issue);
                showUIMessage(message);

                //Add data decide if show the loading footer
                postsByAuthorAdapter.addData(null);

                //Log
                Timber.i(t.toString());
            }
        });
    }

    public void updatePageById(final Context context, int pageId){
        Call<List<Page>> pageCall = retrofit.create(UnitedExplanationsWPService.class).getPageById(pageId);
        pageCall.enqueue(new Callback<List<Page>>() {
            @Override
            public void onResponse(Call<List<Page>> call, Response<List<Page>> response) {
                if (response.body() != null){
                    Page page = response.body().get(0);
                    IntentController.updatePageIntent(context,page);
                }
            }

            @Override
            public void onFailure(Call<List<Page>> call, Throwable t) {
                /* TO DO */
                Timber.e(t.toString());
            }
        });

    }

    public void startPageById(final Context context, int pageId){
        Call<List<Page>> pageCall = retrofit.create(UnitedExplanationsWPService.class).getPageById(pageId);
        pageCall.enqueue(new Callback<List<Page>>() {
            @Override
            public void onResponse(Call<List<Page>> call, Response<List<Page>> response) {
                if (response.body() != null){
                    Page page = response.body().get(0);
                    IntentController.startPageWithIntent(context,page);
                }
            }

            @Override
            public void onFailure(Call<List<Page>> call, Throwable t) {
                /* TO DO */
                Timber.e(t.toString());
            }
        });

    }

    public void updatePageBySlug(String pageSlug){
        Call<List<Page>> pageCall = retrofit.create(UnitedExplanationsWPService.class).getPageBySlug(pageSlug);
        pageCall.enqueue(new Callback<List<Page>>() {
            @Override
            public void onResponse(Call <List<Page>> call, Response<List<Page>> response) {
                if (response.body() != null){
                    Page page = response.body().get(0);
                    IntentController.updatePageIntent(context,page);

                }
            }

            @Override
            public void onFailure(Call<List<Page>> call, Throwable t) {
                /* TO DO */
                Timber.e(t.toString());
            }
        });
    }

    public void startPageBySlug(String pageSlug){
        Call<List<Page>> pageCall = retrofit.create(UnitedExplanationsWPService.class).getPageBySlug(pageSlug);
        pageCall.enqueue(new Callback<List<Page>>() {
            @Override
            public void onResponse(Call <List<Page>> call, Response<List<Page>> response) {
                if (response.body() != null){
                    Page page = response.body().get(0);
                    IntentController.startPageWithIntent(context,page);
                }
            }
            @Override
            public void onFailure(Call<List<Page>> call, Throwable t) {
                /* TO DO */
                Timber.e(t.toString());
            }
        });
    }

    public void updatePageBySlugAndWebView(final Context context, String pageSlug){
        Call<List<Page>> pageCall = retrofit.create(UnitedExplanationsWPService.class).getPageBySlug(pageSlug);
        pageCall.enqueue(new Callback<List<Page>>() {
            @Override
            public void onResponse(Call <List<Page>> call, Response<List<Page>> response) {
                if (response.body() != null){
                    Page page = response.body().get(0);
                    IntentController.updatePageIntent(context,page);
                }
            }

            @Override
            public void onFailure(Call<List<Page>> call, Throwable t) {
                /* TO DO */
                Timber.e(t.toString());
            }
        });
    }



    public void startPostBySlug(String pageSlug) {
        Call<List<Post>> pageCall = retrofit.create(UnitedExplanationsWPService.class).getPostBySlug(pageSlug);
        pageCall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call <List<Post>> call, Response<List<Post>> response) {
                if (response.body() != null){
                    //Get Post
                    Post post = response.body().get(0);
                    IntentController.launchPostActivity(context,post,null);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                /* TO DO */
                Timber.e(t.toString());
            }
        });
    }

    public void getAuthorBySlug(String slug) {
        //Not posible without OAuth Activated
    }

    private void showUIMessage(String message) {
        Intent it = new Intent("EVENT_SNACKBAR");
        if (!TextUtils.isEmpty(message))
            it.putExtra("snackbar_message",message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(it);
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
