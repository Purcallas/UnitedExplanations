package org.unitedexplanations.network;

import org.unitedexplanations.model.wpjson.common.Media;
import org.unitedexplanations.model.wpjson.main.Category;
import org.unitedexplanations.model.wpjson.main.Page;
import org.unitedexplanations.model.wpjson.main.Post;
import org.unitedexplanations.model.wpjson.main.Tag;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static org.unitedexplanations.network.UnitedExplanationsWPService.BASE_SERVICE;

/**
 * Created by victor on 6/7/17.
 */

public interface UnitedExplanationsWPService {

    final String BASE_SERVICE = "/wp-json/wp/v2";



    @GET(BASE_SERVICE+"/posts?_embed&categories_exclude=26,27,28,23,24")
    Call<List<Post>> getPostsWithPage(@Query("page") int page);

    @GET(BASE_SERVICE+"/posts?_embed&categories_exclude=26,27,28,23,24")
    Call<List<Post>> getPostsWithPageAndTag(@Query("page") int page, @Query("categories") int category);

    @GET(BASE_SERVICE+"/posts?_embed&categories_exclude=26,27,28,23,24")
    Call<List<Post>> getPostsWithPageAndAuthor(@Query("page") int page, @Query("author") int author);

    @GET(BASE_SERVICE+"/media/{featured_media}?_embed")
    Call<List<Media>> getMediaWithPost(@Path("featured_media") int featured_media);

    @GET(BASE_SERVICE+"/pages/{page_id}?_embed")
    Call<List<Page>> getPageById(@Path("page_id") int page_id);

    @GET(BASE_SERVICE+"/pages?_embed")
    Call<List<Page>> getPageBySlug(@Query("slug") String slug);

    @GET(BASE_SERVICE+"/categories/{category_id}?_embed")
    Call<List<Category>> getCategoryById(@Path("category_id") int category_id);

    @GET(BASE_SERVICE+"/categories?_embed")
    Call<List<Category>> getCategoryBySlug(@Query("slug") String slug);

    @GET(BASE_SERVICE+"/tags/{tag_id}?_embed")
    Call<List<Tag>> getTagById(@Path("tag_id") int category_id);

    @GET(BASE_SERVICE+"/tags?_embed")
    Call<List<Tag>> getTagsBySlug(@Query("slug") String slug);

    @GET(BASE_SERVICE+"/posts?_embed")
    Call<List<Post>> getPostBySlug(@Query("slug") String slug);
}
