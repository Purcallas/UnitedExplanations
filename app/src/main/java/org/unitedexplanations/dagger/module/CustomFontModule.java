package org.unitedexplanations.dagger.module;

import android.content.Context;
import android.graphics.Typeface;
import android.provider.FontsContract;

import org.unitedexplanations.dagger.libs.CustomTypeface;
import org.unitedexplanations.dagger.qualifier.ApplicationContext;
import org.unitedexplanations.dagger.scope.ApplicationScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by victor on 13/7/17.
 */

@Module(includes = {ContextModule.class})
public class CustomFontModule {

    @Provides
    @ApplicationScope
    public CustomTypeface providesCustomTypeface(@ApplicationContext Context context){
      return new CustomTypeface(context);
    }


}
