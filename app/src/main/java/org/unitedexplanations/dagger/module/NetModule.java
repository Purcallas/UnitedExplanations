package org.unitedexplanations.dagger.module;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;

import org.unitedexplanations.dagger.scope.ApplicationScope;
import org.unitedexplanations.system.App;

import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by victor on 6/7/17.
 */

@Module
public class NetModule {
    String mBaseUrl;

    public NetModule(String mBaseUrl) {
        this.mBaseUrl = mBaseUrl;
    }


    @Provides
    @ApplicationScope
    public Cache provideHttpCache(Application application) {
        int cacheSize = 20 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @ApplicationScope
    public Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Named("Okttp3CacheControl")
    @ApplicationScope
    public OkHttpClient provideOkhttpClientWithCacheControl(final Application application, Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();

        //Decide the cache control depending the internet connection
        client.cache(cache).addInterceptor(new Interceptor() {
            @Override public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                if (isNetworkAvailable(application.getApplicationContext())) {
                    request = request.newBuilder().header("Cache-Control", "public, max-age= 300").build();
                } else {
                    request = request.newBuilder().header("Cache-Control", "public, only-if-cached").build();
                }
                return chain.proceed(request);
            }
        });
        return client.build();
    }

    @Provides
    @Named("Okttp3")
    @ApplicationScope
    public OkHttpClient provideOkhttpClient(final Application application, Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(cache);
        return client.build();
    }

    @Provides
    @ApplicationScope
    //Mantain the usual cache policy for the images
    //Downloader is used by picasso
    public OkHttp3Downloader okHttp3Downloader(@Named("Okttp3") OkHttpClient okHttpClient) {
        return new OkHttp3Downloader(okHttpClient);
    }

    @Provides
    @ApplicationScope
    public Retrofit provideRetrofit(Gson gson,@Named("Okttp3CacheControl") OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @ApplicationScope
    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
