package org.unitedexplanations.dagger.module;

import android.content.Context;

import org.unitedexplanations.dagger.libs.InternalMemory;
import org.unitedexplanations.dagger.qualifier.ApplicationContext;
import org.unitedexplanations.dagger.scope.ApplicationScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by victor on 6/7/17.
 */

@Module(includes = {ContextModule.class})
public class InternalMemoryModule {

    @Provides
    @ApplicationScope
    public InternalMemory provideInternalMemory(@ApplicationContext Context context) {
        return new InternalMemory(context);
    }

}
