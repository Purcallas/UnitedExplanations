package org.unitedexplanations.dagger.module;

import android.content.Context;

import org.unitedexplanations.dagger.qualifier.ApplicationContext;
import org.unitedexplanations.dagger.scope.ApplicationScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by victor on 6/7/17.
 */

@Module
public class ContextModule {

    private final Context context;

    public ContextModule(Context context) {
        this.context = context.getApplicationContext();
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context context() {
        return context;
    }
}
