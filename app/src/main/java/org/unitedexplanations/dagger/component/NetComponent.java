package org.unitedexplanations.dagger.component;

import com.squareup.picasso.Picasso;

import org.unitedexplanations.controller.ActivityNetworkController;
import org.unitedexplanations.dagger.module.AppModule;
import org.unitedexplanations.dagger.module.NetModule;
import org.unitedexplanations.dagger.module.PicassoModule;
import org.unitedexplanations.dagger.scope.ApplicationScope;
import org.unitedexplanations.ui.activity.AuthorActivity;
import org.unitedexplanations.ui.activity.PostActivity;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by victor on 6/7/17.
 */

@ApplicationScope
@Component(modules = {AppModule.class, NetModule.class, PicassoModule.class})
public interface NetComponent {

    //Activity Injection
    void inject(ActivityNetworkController activityNetworkController);
    void inject(PostActivity postActivity);
    void inject(AuthorActivity authorActivity);

    Picasso getPicasso();

    Retrofit getRetrofit();

}
