package org.unitedexplanations.dagger.component;

import android.content.Context;
import android.graphics.Typeface;

import org.unitedexplanations.dagger.libs.CustomTypeface;
import org.unitedexplanations.dagger.module.AppModule;
import org.unitedexplanations.dagger.module.CustomFontModule;
import org.unitedexplanations.dagger.qualifier.ApplicationContext;
import org.unitedexplanations.dagger.scope.ApplicationScope;

import javax.inject.Named;

import dagger.Component;
import dagger.Provides;

/**
 * Created by victor on 13/7/17.
 */

@ApplicationScope
@Component(modules = {AppModule.class, CustomFontModule.class})
public interface StyleComponent {

    CustomTypeface getCustomTypeface();
}
