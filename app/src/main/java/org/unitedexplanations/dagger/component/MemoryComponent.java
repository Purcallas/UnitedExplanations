package org.unitedexplanations.dagger.component;

import org.unitedexplanations.dagger.libs.InternalMemory;
import org.unitedexplanations.dagger.module.AppModule;
import org.unitedexplanations.dagger.module.InternalMemoryModule;
import org.unitedexplanations.dagger.scope.ApplicationScope;

import dagger.Component;

/**
 * Created by victor on 13/7/17.
 */

@ApplicationScope
@Component(modules = {AppModule.class, InternalMemoryModule.class})
public interface MemoryComponent {

    InternalMemory getInternalMemory();

}
