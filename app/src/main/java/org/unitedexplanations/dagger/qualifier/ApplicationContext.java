package org.unitedexplanations.dagger.qualifier;

/**
 * Created by victor on 6/7/17.
 */

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
