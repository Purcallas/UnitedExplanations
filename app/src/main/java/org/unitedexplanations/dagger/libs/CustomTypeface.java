package org.unitedexplanations.dagger.libs;

import android.content.Context;
import android.graphics.Typeface;


/**
 * Created by victor on 22/7/17.
 */

public class CustomTypeface {

    private Context context;

    final String TYPEFACE_BOLD = "OpenSans-Bold.ttf";
    final String TYPEFACE_BOLD_ITALIC = "OpenSans-BoldItalic.ttf";
    final String TYPEFACE_EXTRA_BOLD = "OpenSans-ExtraBold.ttf";
    final String TYPEFACE_EXTRA_BOLD_ITALIC = "OpenSans-ExtraBoldItalic.ttf";
    final String TYPEFACE_ITALIC = "OpenSans-Italic.ttf";
    final String TYPEFACE_LIGHT = "OpenSans-Light.ttf";
    final String TYPEFACE_LIGHT_ITALIC = "OpenSans-LightItalic.ttf";
    final String TYPEFACE_REGULAR = "OpenSans-Regular.ttf";
    final String TYPEFACE_SEMI_BOLD = "OpenSans-SemiBold.ttf";
    final String TYPEFACE_SEMI_BOLD_ITALIC = "OpenSans-SemiBoldItalic.ttf";

    final String TYPEFACE2_BOLD = "HelveticaNeueLTStd-BdCn.otf";
    final String TYPEFACE2_BOLD_ITALIC = "OHelveticaNeueLTStd-BdCnO.otf";
    final String TYPEFACE2_EXTRA_BOLD = "HelveticaNeueLTStd-BlkCn.otf";
    final String TYPEFACE2_EXTRA_BOLD_ITALIC = "HelveticaNeueLTStd-BlkCnO.otf";
    final String TYPEFACE2_ITALIC = "HelveticaNeueLTStd-CnO.otf";
    final String TYPEFACE2_LIGHT = "HelveticaNeueLTStd-LtCn.otf";
    final String TYPEFACE2_LIGHT_ITALIC = "HelveticaNeueLTStd-LtCnO.otf";
    final String TYPEFACE2_REGULAR = "HelveticaNeueLTStd-Cn.otf";
    final String TYPEFACE2_SEMI_BOLD = "HelveticaNeueLTStd-MdCn.otf";
    final String TYPEFACE2_SEMI_BOLD_ITALIC = "HelveticaNeueLTStd-MdCnO.otf";

    final String TYPEFACE3_BOLD = "HelveticaNeueLTStd-Bd.otf";
    final String TYPEFACE3_BOLD_ITALIC = "HelveticaNeueLTStd-BdIt.otf";
    final String TYPEFACE3_EXTRA_BOLD = "HelveticaNeueLTStd-Blk.otf";
    final String TYPEFACE3_EXTRA_BOLD_ITALIC = "HelveticaNeueLTStd-BlkIt.otf";
    final String TYPEFACE3_ITALIC = "HelveticaNeueLTStd-It.otf";
    final String TYPEFACE3_LIGHT = "HelveticaNeueLTStd-Lt.otf";
    final String TYPEFACE3_LIGHT_ITALIC = "HelveticaNeueLTStd-LtIt.otf";
    final String TYPEFACE3_REGULAR = "HelveticaNeueLTStd-Lt.otf";
    final String TYPEFACE3_SEMI_BOLD = "HelveticaNeueLTStd-Md.otf";
    final String TYPEFACE3_SEMI_BOLD_ITALIC = "HelveticaNeueLTStd-MdIt.otf";


    public CustomTypeface(Context context) {
        this.context = context;
    }

    public Typeface getBold(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_BOLD);

    }

    public Typeface getBoldItalic(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_BOLD_ITALIC);

    }

    public Typeface getExtraBold(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_EXTRA_BOLD);

    }

    public Typeface getExtraBoldItalic(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_EXTRA_BOLD_ITALIC);

    }

    public Typeface getItalic(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_ITALIC);

    }

    public Typeface getLight(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_LIGHT);

    }

    public Typeface getLightItalic(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_LIGHT_ITALIC);

    }

    public Typeface getRegular(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_REGULAR);

    }

    public Typeface getSemiBold(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_SEMI_BOLD);

    }

    public Typeface getSemiBoldItalic(){

            return android.graphics.Typeface.createFromAsset(context.getAssets(),"font/"+TYPEFACE_SEMI_BOLD_ITALIC);

    }

    public String getBoldString(){
        return TYPEFACE_BOLD;
    }

    public String getBoldItalicString(){
        return TYPEFACE_BOLD_ITALIC;
    }

    public String getExtraBoldString(){
        return TYPEFACE_EXTRA_BOLD;
    }

    public String getExtraBoldItalicString(){
        return TYPEFACE_EXTRA_BOLD_ITALIC;
    }

    public String getItalicString(){
        return TYPEFACE_ITALIC;
    }

    public String getLightString(){
        return TYPEFACE_LIGHT;
    }

    public String getLightItalicString(){
        return TYPEFACE_LIGHT_ITALIC;
    }

    public String getRegularString(){
        return TYPEFACE_REGULAR;
    }

    public String getSemiBoldString(){
        return TYPEFACE_SEMI_BOLD;
    }

    public String getSemiBoldItalicString(){
        return TYPEFACE_SEMI_BOLD_ITALIC;
    }

    public Typeface getBold(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_BOLD);

            case 2:
                if (TYPEFACE2_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_BOLD);
            case 3:
                if (TYPEFACE3_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_BOLD);
            default:
                return getBold();
        }
    }

    public Typeface getBoldItalic(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_ITALIC);
            case 2:
                if (TYPEFACE2_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_ITALIC);
            case 3:
                if (TYPEFACE3_BOLD_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_BOLD_ITALIC);
            default:
                return getBoldItalic();
        }
    }

    public Typeface getExtraBold(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_EXTRA_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_EXTRA_BOLD);
            case 2:
                if (TYPEFACE2_EXTRA_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_EXTRA_BOLD);
            case 3:
                if (TYPEFACE3_EXTRA_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_EXTRA_BOLD);
            default:
                return getExtraBold();
        }
    }

    public Typeface getExtraBoldItalic(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_EXTRA_BOLD_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_EXTRA_BOLD_ITALIC);

            case 2:
                if (TYPEFACE2_EXTRA_BOLD_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_EXTRA_BOLD_ITALIC);
            case 3:
                if (TYPEFACE3_EXTRA_BOLD_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_EXTRA_BOLD_ITALIC);
            default:
                return getExtraBoldItalic();
        }
    }

    public Typeface getItalic(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_ITALIC);

            case 2:
                if (TYPEFACE2_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_ITALIC);
            case 3:
                if (TYPEFACE3_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_ITALIC);
            default:
                return getItalic();
        }
    }

    public Typeface getLight(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_LIGHT == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_LIGHT);

            case 2:
                if (TYPEFACE2_LIGHT == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_LIGHT);
            case 3:
                if (TYPEFACE3_LIGHT == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_LIGHT);
            default:
                return getLight();
        }

    }

    public Typeface getLightItalic(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_LIGHT_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_LIGHT_ITALIC);
            case 2:
                if (TYPEFACE2_LIGHT_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_LIGHT_ITALIC);
            case 3:
                if (TYPEFACE3_LIGHT_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_LIGHT_ITALIC);
            default:
                return getLightItalic();
        }

    }

    public Typeface getRegular(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_REGULAR == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_REGULAR);
            case 2:
                if (TYPEFACE2_REGULAR == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_REGULAR);
            case 3:
                if (TYPEFACE3_REGULAR == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_REGULAR);
            default:
                return getRegular();
        }
    }

    public Typeface getSemiBold(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_SEMI_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_SEMI_BOLD);
            case 2:
                if (TYPEFACE2_SEMI_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_SEMI_BOLD);
            case 3:
                if (TYPEFACE3_SEMI_BOLD == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_SEMI_BOLD);
            default:
                return getSemiBold();
        }
    }

    public Typeface getSemiBoldItalic(int typeFace){
        switch (typeFace) {
            case 1:
                if (TYPEFACE_SEMI_BOLD_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE_SEMI_BOLD_ITALIC);
            case 2:
                if (TYPEFACE2_SEMI_BOLD_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE2_SEMI_BOLD_ITALIC);
            case 3:
                if (TYPEFACE3_SEMI_BOLD_ITALIC == null) {
                    return android.graphics.Typeface.defaultFromStyle(Typeface.BOLD);
                }
                return android.graphics.Typeface.createFromAsset(context.getAssets(), "font/" + TYPEFACE3_SEMI_BOLD_ITALIC);
            default:
                return getSemiBoldItalic();
        }
    }

    public String getBoldString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_BOLD;
            case 2:
                return TYPEFACE2_BOLD;
            case 3:
                return TYPEFACE3_BOLD;
            default:
                return getBoldString();
        }
    }

    public String getBoldItalicString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_BOLD_ITALIC;
            case 2:
                return TYPEFACE2_BOLD_ITALIC;
            case 3:
                return TYPEFACE3_BOLD_ITALIC;
            default:
                return getBoldItalicString();
        }
    }

    public String getExtraBoldString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_EXTRA_BOLD;
            case 2:
                return TYPEFACE2_EXTRA_BOLD;
            case 3:
                return TYPEFACE3_EXTRA_BOLD;
            default:
                return getExtraBoldString();
        }
    }

    public String getExtraBoldItalicString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_EXTRA_BOLD_ITALIC;
            case 2:
                return TYPEFACE2_EXTRA_BOLD_ITALIC;
            case 3:
                return TYPEFACE3_EXTRA_BOLD_ITALIC;
            default:
                return getExtraBoldItalicString();
        }
    }

    public String getItalicString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_ITALIC;
            case 2:
                return TYPEFACE2_ITALIC;
            case 3:
                return TYPEFACE3_ITALIC;
            default:
                return getItalicString();
        }
    }

    public String getLightString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_LIGHT;
            case 2:
                return TYPEFACE2_LIGHT;
            case 3:
                return TYPEFACE3_LIGHT;
            default:
                return getLightItalicString();
        }
    }

    public String getLightItalicString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_LIGHT_ITALIC;
            case 2:
                return TYPEFACE2_LIGHT_ITALIC;
            case 3:
                return TYPEFACE3_LIGHT_ITALIC;
            default:
                return getLightItalicString();
        }
    }

    public String getRegularString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_REGULAR;
            case 2:
                return TYPEFACE2_REGULAR;
            case 3:
                return TYPEFACE3_REGULAR;
            default:
                return getRegularString();
        }
    }

    public String getSemiBoldString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_SEMI_BOLD;
            case 2:
                return TYPEFACE2_SEMI_BOLD;
            case 3:
                return TYPEFACE3_SEMI_BOLD;
            default:
                return getSemiBoldItalicString();
        }
    }

    public String getSemiBoldItalicString(int typeFace){
        switch (typeFace) {
            case 1:
                return TYPEFACE_SEMI_BOLD_ITALIC;
            case 2:
                return TYPEFACE2_SEMI_BOLD_ITALIC;
            case 3:
                return TYPEFACE3_SEMI_BOLD_ITALIC;
            default:
                return getSemiBoldItalicString();
        }
    }

}
