package org.unitedexplanations.dagger.libs;

/**
 * Created by victor on 21/7/17.
 */

import android.content.Context;
import android.content.SharedPreferences;

import org.unitedexplanations.R;

public class InternalMemory {
    private android.content.SharedPreferences config;
    private android.content.SharedPreferences.Editor configEditor;

    private static String NAME;

    public InternalMemory(Context context) {
        NAME = context.getResources().getString(R.string.app_name);
        config =  context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        configEditor = config.edit();
    }

    public void putString(String key, String value) {
        configEditor.putString(key, value);
        configEditor.commit();
    }

    public void putLong(String key, long value) {
        configEditor.putLong(key, value);
        configEditor.commit();
    }

    public void putInt(String key, int value) {
        configEditor.putInt(key, value);
        configEditor.commit();
    }

    public void putBoolean(String key, boolean value) {
        configEditor.putBoolean(key, value);
        configEditor.commit();
    }


    public String getString(String key) {
        return config.getString(key, "");
    }

    public long getLong(String key) {
        return config.getLong(key, 0);
    }

    public int getInt(String key) {
        return config.getInt(key, 0);
    }

    public boolean getBoolean(String key) {
        return config.getBoolean(key, false);
    }


    public boolean contains(String key) {
        return config.contains(key);
    }

    public void remove(String key) {
        configEditor.remove(key);
        configEditor.commit();
    }

    public void clearAll() {
        configEditor.clear();
        configEditor.commit();
    }

}
